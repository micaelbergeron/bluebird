using UnityEngine;
using System.Collections;

public class LegController : MonoBehaviour {
	
	public float TorqueFactor = 1.0f;
	public float TranslationFactor = 1.0f;
	
	// Update is called once per frame
	void Update () {
		float x = Input.GetAxis("Mouse X");
		float y = Input.GetAxis("Mouse Y");
		
		if (Input.GetMouseButtonDown(0)) 
		{
			mouseToTranslation(x, y);
		} 
		else if (Input.GetMouseButtonDown(1)) 
		{
			mouseToTorque(x, y);
		}			
	}
	
	private void mouseToTorque(float x, float y)
	{
		transform.RotateAround(Vector3.right, x);
		transform.RotateAround(Vector3.forward, y);
	}
	
	private void mouseToTranslation(float x, float y) 
	{
		transform.Translate(y, 0, x);
	}
}
