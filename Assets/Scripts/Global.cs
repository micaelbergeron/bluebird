using UnityEngine;
using System.Collections;

public class Global : MonoBehaviour {

	// Use this for initialization
	void Awake() 
	{
		fastJSON.JSON.Instance.Parameters.UseExtensions = false; // set globally
		fastJSON.JSON.Instance.Parameters.SerializeNullValues = false;
	}
}
