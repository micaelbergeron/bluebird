using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SVector2 
{
	public float x;
	public float y;
	
	public SVector2(Vector2 vector)
	{
		x = vector.x;
		y = vector.y;
	}
	
	public Vector2 asVector2
	{
		get { return new Vector2(x,y); }
	}
}

[Serializable]
public class SVector3 : SVector2 {
	public float z;
	private static int _castDone = 0;
	
	public SVector3(Vector3 vector) 
		: base(vector)
	{
		z = vector.z;
	}
	
	public static SVector3 FromDictionary(IDictionary dict)
	{
	
		SVector3 v = new SVector3(new Vector3());
		try {
			v.x = (float)double.Parse((string)dict["x"]);
			v.y = (float)double.Parse((string)dict["y"]);
			v.z = (float)double.Parse((string)dict["z"]);
			_castDone++;
		} catch (Exception e) {
			Debug.Log("Failed after " + _castDone + " deserialization.");
			Debug.Log(dict["x"].ToString());
		}
		
		return v;
	}
	
	public Vector3 asVector3
	{
		get { return new Vector3(x,y,z); }
	}
}

[Serializable]
public class SVector4 : SVector3 {
	public float w;
	
	public SVector4(Vector4 vector) 
		: base(vector)
	{
		w = vector.w;
	}
	
	public Vector4 asVector4
	{
		get { return new Vector4(x,y,z,w); }
	}
}