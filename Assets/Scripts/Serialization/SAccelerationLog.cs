using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SAccelerationLog {
	
	public string file;
	
	/// <summary>
	/// The idle acceleration value used for this log.
	/// </summary>
	public SVector3 idle;
	public List<SAccelerationEntry> entries = new List<SAccelerationEntry>();
	
	public static SAccelerationLog FromJson(string json) 
	{
		IDictionary log = MiniJSON.Json.Deserialize(json) as IDictionary;
		
		return FromDictionary(log);
	}
	
	public static SAccelerationLog FromDictionary(IDictionary dict)
	{
		SAccelerationLog log = new SAccelerationLog();
		
		log.file = (string)dict["file"];
		log.idle = SVector3.FromDictionary((IDictionary)dict["idle"]);
		
		foreach (IDictionary entry in (IList)dict["entries"]) 
		{
			log.entries.Add(SAccelerationEntry.FromDictionary(entry));
		}
				
		return log;
	}
}
