using UnityEngine;
using System.Collections;
using MiniJSON;
using System.IO;

public interface IJsonSerializable
{
	bool JsonDeserialize(StreamReader reader);
	bool JsonSerialize(StreamWriter writer);
}
