using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SAccelerationEntry {
	public SVector3 accel 
	{ 
		get; 
		set; 
	}
	public double time { get; set; }
	
	public static SAccelerationEntry FromDictionary(IDictionary dict) 
	{
		SAccelerationEntry entry = new SAccelerationEntry();
		
		entry.time = (double)dict["timestamp"];		
		entry.accel = SVector3.FromDictionary(dict);
		return entry;
	}
}
