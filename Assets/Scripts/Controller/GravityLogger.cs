using UnityEngine;
using System.IO;
using System.Text;
using System.Collections;
using System.Threading;

public class GravityLogger : MonoBehaviour {
	
	private string basedir = "Assets/Resources/";
	public string OutputFileName;
	
	/// <summary>
	/// The polling rate, in hertz.
	/// </summary>
	public int PollingRate = 20;
	
	// Thread info
	Vector3 currentVelocity = Vector3.zero;
	Vector3 lastvelocity = Vector3.zero;
	float time = 0;
	
	string format = "{0:0.000}\t{1}";
	int sleep = 0;
			
	StreamWriter log_writer = null;
	Thread tLogger;
	
	// Use this for initialization
	void Start () {
		sleep = 1000 / PollingRate;
		log_writer = new StreamWriter(basedir + OutputFileName);
		
		writeHeader(log_writer);
		tLogger = new Thread(runLogAcceleration);
		tLogger.Start ();
	}
			
	void OnDisable()
	{
		tLogger.Abort();
		log_writer.Dispose();
	}
	
	void FixedUpdate()
	{
		lastvelocity = currentVelocity;
		currentVelocity = rigidbody.velocity;
		time = Time.time;
	}
	
	void writeHeader(StreamWriter writer)
	{
		writer.WriteLine("Capture started at " + Time.time.ToString());
		writer.Flush();
	}
		
	void runLogAcceleration() {	
		double lastTime = -1.0f;
		while (true) {
			if (lastTime < time) {
				lastTime = time;
				SVector3 acceleration = new SVector3((currentVelocity - lastvelocity) / (sleep / 1000.0f));
				SAccelerationEntry entry = new SAccelerationEntry() 
				{
					accel = acceleration,
					time = time
				};
				
				lock (log_writer) {				
					log_writer.WriteLine(fastJSON.JSON.Instance.ToJSON(entry));	
					log_writer.Flush();
				}
				Thread.Sleep(sleep);	
			}
		}
	}
}
