using UnityEngine;
using System.Collections;
using System.IO;

[RequireComponent(typeof(Rigidbody))]
public class AccelerationController : MonoBehaviour {
	
	public string filepath;	
	public Vector3 acceleration;
	public float scale = 1.0f;
	public float timeScale = 1.0f;
	public bool compensateGravity = true;
	public float MaxGs = Physics.gravity.magnitude;
	
	public Vector3 threshold = new Vector3(0.05f, 0.05f, 0.05f);
		
	private Vector3 initialPos;
	private SAccelerationLog log;
	private int _cursor;
	private bool loaded = false;
	private float timeRestart = 0;
	
	// Use this for initialization
	void Start () 
	{
		initialPos = transform.position;
		Time.timeScale = timeScale;
		_cursor = 0;
		try {
			string logText = File.ReadAllText(filepath);
			log = SAccelerationLog.FromJson(logText);
			Debug.Log("Acceleration log loaded, found " + log.entries.Count + " entries.");
			loaded = true;
		} catch(IOException e) {
			Debug.LogException(e);
		}
		
	}
	
		
	// Update is called once per frame
	void FixedUpdate () 
	{		
		if (loaded) {
			Vector3 accel = MaxGs * Physics.gravity.magnitude * calculateAcceleration(Time.fixedTime * 1000);
			if (isBrake(accel)) {
				transform.position = initialPos;
			} else {
				rigidbody.AddForce(scale * accel, ForceMode.Acceleration);
			}			
		}
	}
	
	private Vector3 calculateAcceleration(float time)
	{
		if (_cursor >= log.entries.Count) {
			_cursor = 0;
			timeRestart = time;
			return Vector3.zero;
		}
		
		SAccelerationEntry currentEntry = log.entries[_cursor++];
		while( (currentEntry.time < (time - timeRestart)) && (_cursor < log.entries.Count))
		{
			currentEntry = log.entries[_cursor++];
		}
		
		if (isNoise(currentEntry.accel.asVector3)) {
			return Vector3.zero;
		} else {
			return currentEntry.accel.asVector3 - log.idle.asVector3;
		}
	}
	
	private bool isBrake(Vector3 accel) {
		float theta = Vector3.Angle(Vector3.right, accel);
		return (theta > 180) || (theta < 0);
	}
		
	private bool isNoise(Vector3 accel) {
		Vector3 min = log.idle.asVector3 - threshold;
		Vector3 max = log.idle.asVector3 + threshold;
		
		return (min.sqrMagnitude < accel.sqrMagnitude && max.sqrMagnitude > accel.sqrMagnitude);
	}
}
