using UnityEngine;
using System.Collections;

public class Kicker : MonoBehaviour {
	
	private Vector3 _originalPos;
	private Quaternion _originalRot;
	
	public float kickforce = 1.0f;
	public float dribbleforce = 0.1f;
	
	public Transform kickable;
	
	// Use this for initialization
	void Start () {
		if (kickable == null) 
			throw new MissingReferenceException("An object must be set as kickable for a kicker to work.");
		
		_originalPos = kickable.position;
		_originalRot = kickable.rotation;
	}
	
	// Update is called once per frame
	void Update () {		
		if (Input.GetMouseButtonDown(0)) {
			Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit info;
			if (Physics.Raycast(r, out info)) {				
				if ((info.transform == kickable) && (info.rigidbody != null)) {
					
					Vector3 contact = info.point;
					Vector3 kickForce = (contact - transform.position).normalized * kickforce;
					
					info.rigidbody.AddForceAtPosition(kickForce, contact, ForceMode.Impulse);
					info.rigidbody.useGravity = true;
				}
			}
		}
		if (Input.GetMouseButtonDown(1)) {
			Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit info;
			if (Physics.Raycast(r, out info)) {
				if ((info.transform == kickable) && (info.rigidbody != null)) {
					// find the middle of the ball, and add an impulse from ground up
					Vector3 ball_center = info.transform.position;
					Vector3 contact = info.point;
				
					// we dont want the ball to be pushed away by a dribble
					Vector3 inside_dribble = (ball_center - contact).normalized;
					inside_dribble.z = 0;
					Vector3 dribble = dribbleforce * inside_dribble;	
				
					info.rigidbody.AddForceAtPosition(dribble, ball_center - inside_dribble, ForceMode.Impulse);
					info.rigidbody.useGravity = true;
				}

			}
		}
		
		if (Input.GetButtonDown("Jump")) {		
			Resetable[] resetables = (Resetable[])GameObject.FindSceneObjectsOfType(typeof(Resetable));
			foreach (Resetable r in resetables) {
				r.ResetLastState();
			}
		}
	}
}
