using UnityEngine;
using System.Collections;

public class KickConfig : MonoBehaviour {
	
	private float Angle = 0.0f;
	private float Power = 0.0f;
	
	public AccelerationController kicker = null;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (kicker != null) {
			kicker.scale = Power;
		}
	}
	
	void OnGUI() 
	{
		Rect boxBounds = new Rect(20, 20, 100, 200);
		
		// show the angle, and a fake ball
		GUI.Box(boxBounds,
			"Kick Angle chooser");
		
		Power = GUI.HorizontalSlider(
			new Rect(15, 30, 80, 30),
			Power,
			0,
			50);
				
		Angle = GUI.HorizontalSlider(
			new Rect(15,15,80,30), 
			Angle, 
			0,
			Mathf.PI
		);
				
		// display the angle
		GUIHelper.BeginGroup(boxBounds);
		
		Vector2 translation = Vector2.zero;
		translation.x = boxBounds.width / 2;
		
		Vector2 start = translation;
		start.y = boxBounds.height - 20;
		
		float sinAngle = Mathf.Sin(Angle);
		float cosAngle = Mathf.Cos(Angle);
		
		Vector2 end;
		end.x = start.x * cosAngle + start.y * sinAngle;
		end.y = start.y * cosAngle - start.y * sinAngle;
			
		// draw a reference Up vector, and the Angle vector
		GUIHelper.DrawLine(start, translation, Color.red);
		GUIHelper.DrawLine(start, start + new Vector2(0,1), Color.blue);
		GUIHelper.DrawLine(start, end, Color.white);
		
		GUIHelper.EndGroup();
	}
}
