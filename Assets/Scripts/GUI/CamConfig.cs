using UnityEngine;
using System.Collections;

public class CamConfig : MonoBehaviour {
	
	public Camera orthoCam, perspCam;
	
	// Use this for initialization
	void Start () {
		
	}
	
	void OnGUI() {		
		Rect boxBounds = new Rect(130, 20, 100, 200);
		GUI.Box(boxBounds, "Camera");
		
		if (GUI.Button(new Rect(135, 50, 90, 30), "Orthogonal")) {
			orthoCam.enabled = true;
			perspCam.enabled = false;
		}
		
		if (GUI.Button(new Rect(135, 90, 90, 30), "Perspective")) {
			orthoCam.enabled = false;
			perspCam.enabled = true;
		}
	}
}
