using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Aerodynamic : MonoBehaviour {
	
	public float AverageFluidResistance = 0.02f;
	
	void Update () {
		// Update is called once per frame
		Vector3 liftForce = AverageFluidResistance * Vector3.Cross(rigidbody.angularVelocity, rigidbody.velocity);	
		rigidbody.AddForce(liftForce);
		
		Debug.DrawLine(transform.position, transform.position + liftForce);
	}
	
	void OnCollisionEnter(Collision c) {
		this.rigidbody.useGravity = true;
	}
	
	private void drawVector(Vector3 origin, Vector3 target) {
		GL.PushMatrix();
		GL.Begin(GL.LINES);
		
		GL.LoadIdentity();
		
		GL.Color(Color.green);
		GL.Vertex(origin);
		GL.Vertex(target);
				
				
		GL.End();
		GL.PopMatrix();
	}
}
