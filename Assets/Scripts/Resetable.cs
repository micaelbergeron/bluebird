using UnityEngine;
using System.Collections;

public class Resetable : MonoBehaviour {
	
	private GameObject _initial;
	
	// Use this for initialization
	void Start () {		
		_initial = new GameObject("SavedState");
		Memorize();
	}
	
	public void Memorize() {		
		_initial.transform.position = transform.position;
		_initial.transform.rotation = transform.rotation;
	}
	
	public void ResetLastState() {
		transform.position = _initial.transform.position;
		transform.rotation = _initial.transform.rotation;
		
		if (rigidbody != null) {
			rigidbody.velocity = Vector3.zero;
			rigidbody.angularVelocity = Vector3.zero;
			rigidbody.useGravity = false;
		}
	}
}
