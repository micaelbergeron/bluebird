using UnityEngine;
using System.Collections;

public class RayTracer : MonoBehaviour {
	
	private Vector3[] worldRays= new Vector3[3];
	private Color[] rayColor = new Color[3];
	public int length = 100;
		
	// Use this for initialization
	void Start () {
		worldRays[0] = Vector3.down;
		worldRays[1] = Vector3.right;
		worldRays[2] = Vector3.forward;
		
		rayColor[0] = Color.green;
		rayColor[1] = Color.red;
		rayColor[2] = Color.blue;
	}
	
	// Update is called once per frame
	void Update () {		
		for(int i = 0; i < 3; i++) {
			Debug.DrawRay(transform.position, length * worldRays[i].normalized, rayColor[i]);
		}		
	}
}
