import matplotlib.pyplot as plot;
import json;
import sys;

f = open(sys.argv[1])
print f
rawLog = f.read()
log = json.loads(rawLog)

plot.ylabel('acceleration')
plot.xlabel('time')
plot.title(sys.argv[1] + ' acceleration')

accel = {
    'x': [],
    'y': [],
    'z': []
}
time = []

for entry in log['entries']:
    accel['x'].append(entry['x'])
    accel['z'].append(entry['z'])
    accel['y'].append(entry['y'])
    time.append(entry['timestamp'])

plot.plot(time, accel['x'], 'r')
plot.plot(time, accel['y'], 'g')
plot.plot(time, accel['z'], 'b')
plot.xlim(0, max(time))
plot.ylim(0, 0.80)

plot.savefig(sys.argv[1] + '.svg')