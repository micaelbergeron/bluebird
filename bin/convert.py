import fileinput
import json
import re
import sys
from cgkit.cgtypes import *

idleAccel = vec3(0.465, 0.5, 0.415);
idleReal = vec3(0, -1, 0);

qrot = quat(idleAccel.angle(idleReal), idleAccel.cross(idleReal));
ROT = qrot.toMat4()
G_MAX = vec3(1).length()
print G_MAX

def changeBasis(v, rot_matrix=mat4(1.0), max_g=G_MAX):
    precision = 5
    amplitude = v.length();
    
    v = rot_matrix * v;
    v = max_g * amplitude * v;
    v.x = round(v.x, precision);
    v.y = round(v.y, precision);
    v.z = round(v.z, precision);
    
    return v

# file schema: r(Raw), f(Filtered)
# r(x) f(x) r(z) f(z) r(y) f(y) timestamp
def formatAsJson(filename, start_idx=0, idleValue=idleAccel):
    output = open(filename, 'w')
    
    log = {
        'file': sys.argv[1],
        'idle': {
                 'x': str(idleAccel.x),
                 'y': str(idleAccel.y),
                 'z': str(idleAccel.z)
         },
        'entries': []
    }

    for line in fileinput.input():
        rawlog = line.replace(',','.').split()
        x = float(rawlog[start_idx+0])
        z = float(rawlog[start_idx+2])
        y = float(rawlog[start_idx+4])
        v = vec3(x,y,z)
        
        if (x+y+z) != 0:
            entry = {
                'x': '{:f}'.format(v.x),
                'y': '{:f}'.format(v.y),
                'z': '{:f}'.format(v.z),
                'timestamp': float(rawlog[6])
            }
            log['entries'].append(entry)
    print 'wrote ' + str(len(log['entries']))
    output.write(json.dumps(log, indent=2))
    output.close()

class AccelerationLogEntry:
    def __init__(self, x, y, z, timestamp):
        self.x = x
        self.y = y
        self.z = z
        self.timestamp = timestamp      
        
# idleAccel = {
#    'x': 0.4027375,
#    'y': 0.4682307,
#    'z': 0.4848485
#    }

formatAsJson(sys.argv[1] + '.raw.json')
formatAsJson(sys.argv[1] + '.json', start_idx=1)
